#!/bin/sh
# Pulls upstream honk changes into local hg branch for
# rebasing/merging with main branch.
#
# Requires hg and hg-git to be installed.
set -e

this_dir="$( cd "$( dirname "$0" )/../" && pwd )"

tmp_dir="$this_dir/tmp-honk-dir"
rm -rf "$tmp_dir"
mkdir "$tmp_dir"
hg clone https://humungus.tedunangst.com/r/honk "$tmp_dir"
cd "$tmp_dir"
hg bookmark hg
hg push "$this_dir" || echo 'No upstream changes'
cd "$this_dir"
rm -rf "$tmp_dir"
exec git rebase hg
